// Main.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <memory>
#include <vector>
#include "Hierarchy.h"

using namespace std;


int main()
{
	vector<shared_ptr<Dialog>> t;
	shared_ptr<Dialog> em(new ErrorMessage(true, "ErrorMessage", 100, 50, true));
	shared_ptr<Dialog> pd(new ProgressDialog(true, "ProgressDialog", 150, 75, true));
	shared_ptr<Dialog> fd(new FileDialog(true, "FileDialog", 100, 25, true));
	t.push_back(em);
	t.push_back(pd);
	t.push_back(fd);
	for (int i = 0; i < t.size(); i++)
		cout << t.at(i)->getName() << " " << t.at(i)->sizeHint()<< endl;
    return 0;
}

