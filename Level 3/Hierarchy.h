#pragma once

#include <string>

using namespace std;

class Object
{
	protected:
		string name;
	public:
		Object(){}

		virtual string getName() = 0;
};

class Widget : public Object
{
	protected:
		int cursor, position;
	public:
		Widget(){}

		string getName()
		{
			return name;
		}
};

class Dialog : public Widget
{
	protected:
		bool visible;
	public:
		Dialog(){}

		virtual int sizeHint() = 0;
};

class ProgressDialog : public Dialog
{
	private:
		bool autoClose;
	public:
		ProgressDialog(){}

		ProgressDialog(bool AutoClose, string Name, int Cursor, int Position, bool Visible)
		{
			this->autoClose = AutoClose;
			this->name = Name;
			this->cursor = Cursor;
			this->position = Position;
			this->visible = Visible;
		}

		ProgressDialog(const ProgressDialog &pd)
		{
			this->autoClose = pd.autoClose;
			this->name = pd.name;
			this->cursor = pd.cursor;
			this->position = pd.position;
			this->visible = pd.visible;
		}

		~ProgressDialog()
		{
			cout << "ProgressDialog destroyed!" << endl;
		}

		int sizeHint()
		{
			return this->cursor / this->position;
		}
};

class FileDialog : public Dialog
{
	private:
		bool preview;
	public:
		FileDialog(){}

		FileDialog(bool Preview, string Name, int Cursor, int Position, bool Visible)
		{
			this->preview = Preview;
			this->name = Name;
			this->cursor = Cursor;
			this->position = Position;
			this->visible = Visible;
		}

		FileDialog(const FileDialog &fd)
		{
			this->preview = fd.preview;
			this->name = fd.name;
			this->cursor = fd.cursor;
			this->position = fd.position;
			this->visible = fd.visible;
		}

		~FileDialog()
		{
			cout << "FileDialog destroyed!" << endl;
		}

		int sizeHint()
		{
			return this->cursor/this->position;
		}
};

class ErrorMessage : public Dialog
{
	private:
		bool showOnlyOnce;
	public:
		ErrorMessage(){}

		ErrorMessage(bool ShowOnlyOnce, string Name, int Cursor, int Position, bool Visible)
		{
			this->showOnlyOnce = ShowOnlyOnce;
			this->name = Name;
			this->cursor = Cursor;
			this->position = Position;
			this->visible = Visible;
		}

		ErrorMessage(const ErrorMessage &em)
		{
			this->showOnlyOnce = em.showOnlyOnce;
			this->name = em.name;
			this->cursor = em.cursor;
			this->cursor = em.cursor;
			this->position = em.position;
			this->visible = em.visible;
		}

		~ErrorMessage()
		{
			cout << "ErrorMessage destroyed!"<<endl;
		}

		int sizeHint()
		{
			return this->cursor / this->position;
		}	
};