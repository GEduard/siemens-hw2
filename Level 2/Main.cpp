// Main.cpp : Defines the entry point for the console application.
//

#include <iostream>

using namespace std;

int DeadMethod()
{
	cout << "I will never be printed to the console :("<<endl;
	return 0;
}


int main()
{
	int a = 1, v[5], *t;
	t = new int[5];
	for (int i = 0; i<5; i++)
	{
		v[i] = rand() % 10;
		t[i] = rand() % 10;
	}
	a++;
	cout << a << " " <<v[0]<<endl;
	DeadMethod();
	delete[] t;
	return 0;
}