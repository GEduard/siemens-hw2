1)Create a folder for your CMake soltuion that we want to build. Give it a proper name.

2)In this folder create the following subfolders: out, output, src.

3)In the "src" directory create again 3 folders: MainProgram, DynamicLIB1, DynamicLIB2, StaticLIB

4)The final folder we want to create is one called "Executable". You can place it wherever you want in the main folder, but I recommend placing it in the "out" directory.

5)Go back to the main folder. Here we want to create a "CMakeLists.txt" file, with the following content:

cmake_minimum_required (VERSION 3.7)

project(SiemensHW1L1)

add_subdirectory(src/MainProgram)
add_subdirectory(src/DynamicLIB1)
add_subdirectory(src/DynamicLIB2)
add_subdirectory(src/StaticLIB)


6)Save it, now go to the "src" folder. Place the downloaded files in their corresponding folders(Main.cpp in MainProgram, DynamicLIB1.cpp & DynamicLIB1.h in DynamicLIB1, etc.)

7)Go to the folder "MainProgram" and create a "CMakeLists.txt" with the following content:

project (MainProgram)
add_executable(MainProgram Main.cpp)


8)Go to the folder "DynamicLIB1" and create a "CMakeLists.txt" with the following content:

project (DynamicLIB1)
add_library(DynamicLIB1 SHARED DynamicLIB1.h DynamicLIB1.cpp)


9)Do the same for DyanmicLIB2(but change '1' in the names with '2' obviously). Go to the "StaticLIB" folder and create a "CMakeLists.txt" with the following content:

project (StaticLIB)
add_library(StaticLIB STATIC StaticLIB.h StaticLIB.cpp)

10)Open up CMake, configure and generate the solution. Do not forget to add EXECUTABLE_OUTPUT_PATH and LIBRARY_OUTPUT_PATH(paths), both pointing to the "Executable" folder.

11)Open up the solution in Visual Studio. But we're not done yet. Right click on the "MainProgram" project, go to Properties>Configuration Porperties>C/C++. You should see a line "Addition Include Directories". Add a new path, to the "StaticLIB" project. Then go to References in your Solution Explorer, for the "MainProgram" project and add "StaticLIB" and "DynamicLIB2". Set "MainProgram" as start up project, build the solution and run it. The console should print 3 messages.