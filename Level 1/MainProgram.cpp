// MainProgram.cpp : Defines the entry point for the console application.
//

#include <Windows.h>
#include "StaticLIB.h"

typedef void(__cdecl *CallFirst)();
extern "C" void __cdecl SecondMethod();

int main()
{
	HINSTANCE dModule = LoadLibrary(TEXT("DynamicLIB1"));
	CallFirst dCall = (CallFirst)GetProcAddress(dModule, "FirstMethod");
	if (dCall)
		dCall();
	SecondMethod();
	ThirdMethod();
	FreeLibrary(dModule);
    return 0;
}